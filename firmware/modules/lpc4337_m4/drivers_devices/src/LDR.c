/*
 * LDR.c
 *
 *  Created on: 25 may. 2022
 *      Author: MAGALI KRISTAFOR
 */

#include <stdint.h>
#include <analog_io.h>
#include "uart.h"
#include "uart_stdio.h"
#include "analog_io.h"
#include "timer.h"
#include "systemclock.h"


analog_input_config LDRin;

#define LDR_DARK 1000 /*Resistencia en oscuridad, en KΩ*/
#define LDR_10LUX 15 /*Resistencia en 10 lux, aprox en Kohm*/
#define R_CALIBRACION 47 /*Resistencia calibracion en KΩ*/
#define LUX_NORMAL 500 /*lux normal*/

uint16_t luz_aux_dig;
uint32_t ilum_lux;




void LDR_Init(uint8_t Channel){

	LDRin.input=Channel;
	LDRin.mode=AINPUTS_SINGLE_READ;
	LDRin.pAnalogInput=NULL;



}

bool LDR_ReadLuxIntensity(uint32_t *ilum_lux){

	AnalogInputInit(&LDRin);
	AnalogInputReadPolling(LDRin.input,&luz_aux_dig);
	 *ilum_lux=(uint32_t)((luz_aux_dig*LDR_DARK*10)/(LDR_10LUX*R_CALIBRACION*(1024-luz_aux_dig))); /*el dato digital lo paso en unidades de lux*/
	 	return TRUE;

}


