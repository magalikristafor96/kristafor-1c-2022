/*
 * nivel_tanque.c
 *
 *  Created on: 16 jun. 2022
 *      Author: MAGALI KRISTAFOR
 */
/* Copyright 2019,
 * Electrónica Programable
 * jmrera@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "stdint.h"
#include "stdbool.h"
#include <stdint.h>
#include <analog_io.h>
#include "uart.h"
#include "uart_stdio.h"
#include "analog_io.h"
#include "timer.h"
#include "systemclock.h"
#include "hc_sr04.h"

uint16_t agua_aux;

uint16_t volumen;

#define PI 3.14  //
#define DIAMETRO 95  //diametro en cm
#define ALTURA 140 // altura en cm


void NivelAguaInit(){

	HcSr04Init(GPIO_T_FIL0, GPIO_T_FIL2);//para tanque 1
	HcSr04Init(GPIO_T_FIL1, GPIO_T_FIL3); //para tanqu 2

}

//nivel de agua en litros.

bool ReadNivelAgua(uint16_t *nivel){

	agua_aux=HcSr04ReadDistanceInCentimeters();
	volumen=(float)(PI*((DIAMETRO/2)*(DIAMETRO/2))*ALTURA);

	*nivel=(float)(volumen*0.001);
	return TRUE;

}





