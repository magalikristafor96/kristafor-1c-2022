/* Copyright 2017, Agustin Bassi.
 * All rights reserved.
 *
 * This file is part sAPI library for microcontrollers.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
    Copyright 2001 Georges Menie
    https://www.menie.org/georges/embedded/small_printf_source_code.html
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
	putchar is the only external dependency for this file,
	if you have a working putchar, just remove the following
	define. If the function should be called something else,
	replace outbyte(c) by your own function call.
 */

/*
 * Date: 2017-11-01
 */

/*==================[inclusions]=============================================*/

#include "uart_stdio.h"
#include "math.h"

/*==================[macros and definitions]=================================*/

#define putchar(c)      outbyte(c)
#define PAD_RIGHT       1
#define PAD_ZERO        2
#define PRINT_BUF_LEN   12

/*==================[internal data declaration]==============================*/

static uint8_t Port = SERIAL_PORT_PC;

/*==================[internal functions declaration]=========================*/

static void printchar (char **str, int c);

static int  prints    (char **out, const char *string, int width, int pad);

static int  printi    (char **out, int i, int b, int sg, int width, int pad, int letbase);

static int  print     (char **out, int *varg);

/*==================[internal data definition]===============================*/

void        _outbyte  (int c){
	uint8_t data = c;
	UartSendByte(Port, &data);
}

void        outbyte   (int c){
	static char prev = 0;
	if (c < ' ' && c != '\r' && c != '\n' && c != '\t' && c != '\b')
		return;
	if (c == '\n' && prev != '\r') _outbyte('\r');
	_outbyte(c);
	prev = c;
}

/**
 * Imprime un char en un string o por la UART.
 * Si str es distinto de null (es decir sprintf)
 * entra en el if y guarda el char en str.
 * Sino, llama a putchar, que a fin de cuentas
 * termina sacando un dato por la UART.
 * @param str
 * @param c
 */
static void printchar (char **str, int c){
	extern void putchar(int c);
	if (str) {
		**str = c;
		++(*str);
	}
	else{
		(void)putchar(c);
	}
}

static int  prints    (char **out, const char *string, int width, int pad){
	register int pc = 0, padchar = ' ';

	if (width > 0) {
		register int len = 0;
		register const char *ptr;
		for (ptr = string; *ptr; ++ptr) ++len;
		if (len >= width) width = 0;
		else width -= len;
		if (pad & PAD_ZERO) padchar = '0';
	}
	if (!(pad & PAD_RIGHT)) {
		for ( ; width > 0; --width) {
			printchar (out, padchar);
			++pc;
		}
	}
	for ( ; *string ; ++string) {
		printchar (out, *string);
		++pc;
	}
	for ( ; width > 0; --width) {
		printchar (out, padchar);
		++pc;
	}
	return pc;
}

static int  printi    (char **out, int i, int b, int sg, int width, int pad, int letbase){
	char print_buf[PRINT_BUF_LEN];
	register char *s;
	register int t, neg = 0, pc = 0;
	register unsigned int u = i;

	if (i == 0) {
		print_buf[0] = '0';
		print_buf[1] = '\0';
		return prints (out, print_buf, width, pad);
	}

	if (sg && b == 10 && i < 0) {
		neg = 1;
		u = -i;
	}

	s = print_buf + PRINT_BUF_LEN-1;
	*s = '\0';

	while (u) {
		t = u % b;
		if( t >= 10 )
			t += letbase - '0' - 10;
		*--s = t + '0';
		u /= b;
	}

	if (neg) {
		if( width && (pad & PAD_ZERO) ) {
			printchar (out, '-');
			++pc;
			--width;
		}
		else {
			*--s = '-';
		}
	}

	return pc + prints (out, s, width, pad);
}

static int  print     (char **out, int *varg){
	register int width, pad;
	register int pc = 0;
	register char *format = (char *)(*varg++);
	char scr[2];

	for (; *format != 0; ++format) {
		if (*format == '%') {
			++format;
			width = pad = 0;
			if (*format == '\0') break;
			if (*format == '%') goto out;
			if (*format == '-') {
				++format;
				pad = PAD_RIGHT;
			}
			while (*format == '0') {
				++format;
				pad |= PAD_ZERO;
			}
			for ( ; *format >= '0' && *format <= '9'; ++format) {
				width *= 10;
				width += *format - '0';
			}
			if( *format == 's' ) {
				register char *s = *((char **)varg++);
				pc += prints (out, s?s:"(null)", width, pad);
				continue;
			}
			if( *format == 'd' ) {
				pc += printi (out, *varg++, 10, 1, width, pad, 'a');
				continue;
			}
			if( *format == 'x' ) {
				pc += printi (out, *varg++, 16, 0, width, pad, 'a');
				continue;
			}
			if( *format == 'X' ) {
				pc += printi (out, *varg++, 16, 0, width, pad, 'A');
				continue;
			}
			if( *format == 'u' ) {
				pc += printi (out, *varg++, 10, 0, width, pad, 'a');
				continue;
			}
			if( *format == 'c' ) {
				/* char are converted to int then pushed on the stack */
				scr[0] = *varg++;
				scr[1] = '\0';
				pc += prints (out, scr, width, pad);
				continue;
			}
		}
		else {
			out:
			printchar (out, *format);
			++pc;
		}
	}
	if (out) **out = '\0';
	return pc;
}

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

uint8_t* Uart_itoa(uint32_t val, uint8_t base)
{
	static char buf[32] = {0};

	uint32_t i = 30;

	for(; val && i ; --i, val /= base)

		buf[i] = "0123456789abcdef"[val % base];

	return &buf[i+1];
}

uint8_t* UART_ftoa(uint8_t W, uint8_t D, float data_n){
  float data = fabs(data_n);
  long Wdata = data, tmp;
  static uint8_t buf[10]= {0};
  static uint8_t buf_neg[10]= {0};
  //uint8_t buf[10]={0};
  // get the first whole number and convert it
  uint8_t j =0;

  float dec = data - (long)data;
  buf[j] = Wdata / pow(10, W - 1) + '0';
  tmp = Wdata % (long)pow(10, W - 1);

  //now get the rest of the whole numbers
  for (uint8_t i = 1; i < W; i++)  {
    long factor = pow(10, W - 1 - i);
    buf[i+j] = (tmp / factor) + '0';
    tmp %= factor;
  }

  buf[W] = '.'; // add the decimal point

  // now do the decimal numbers
  for (uint8_t i = 0; i < D; i++)  {
    dec *= 10;
    buf[W + i + 1 ] = ((long)dec % 10) + '0';
  }
  // don't forget the NULL terminator
  buf[W + D + 1] = 0;
  if(data_n<0){
	  buf_neg[0]='-';
	  for (uint8_t i = 0; i < D+W+1; i++)	    {
		  buf_neg[i+1] = buf[i];
	    }
	  return &buf_neg;
  }
  else{return &buf;}
}

bool stdioConfig (uint8_t port){
	Port = port;
	
	serial_config stdio_uart = {Port, 115200, NO_INT};
	UartInit(&stdio_uart);
	return TRUE;
}

int stdioPrintf(uint8_t port, const char *format, ...){
	register int *varg = (int *)(&format);
	Port = port;
	return print(0, varg);
}

int stdioSprintf(char *out, const char *format, ...){
	register int *varg = (int *)(&format);
	return print(&out, varg);
}

/*==================[end of file]============================================*/


