/*
 * nivel_tanque.h
 *
 *  Created on: 16 jun. 2022
 *      Author: MAGALI KRISTAFOR
 */

/* Copyright 2019,
 * Electrónica Programable
 * jmrera@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*! @mainpage Template
 *
 * \section genDesc General Description
 * La aplicacion permite medir el nivel de agua en dos tanques, destinados para el riego de un cultivo.
 * This section describes how the program works.
 *
 * Segun el nivel de agua en los tanques, acciona una bomba para el llenado del tanque 2
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 *
 * * hardConn Hardware Connection
 *tanque1
 * |  Ultrasonidos  |				|
 * |	HC-SR04     |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	Echo	 	| 	GPIO_T_FIL0	|
 * | 	Trigger	 	| 	GPIO_T_FIL2	|
 * | 	GND		 	| 	GND			|
 * | 	VCC		 	| 	VCC			|
 * tanque2
 *  * hardConn Hardware Connection
 *
 * |  Ultrasonidos  |				|
 * |	HC-SR04     |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	Echo	 	| 	GPIO_T_FIL1	|
 * | 	Trigger	 	| 	GPIO_T_FIL3	|
 * | 	GND		 	| 	GND			|
 * | 	VCC		 	| 	VCC			|
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 16/06/2022 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 *
 */


#include <stdint.h>
#include <analog_io.h>



void NivelAguaInit();

bool ReadNivelAgua(uint16_t *nivel);







#ifndef MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_NIVEL_TANQUE_H_
#define MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_NIVEL_TANQUE_H_





#endif /* MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_NIVEL_TANQUE_H_ */
