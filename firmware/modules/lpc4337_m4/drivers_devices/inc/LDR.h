/* Copyright 2019,
 * Electrónica Programable
 * jmrera@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*Descripcion General.
 * Se utiliza conectada en serie con una resistencia de calibracion de 47Kohm, con una alimentacion de 5V.
 * Este driver inicializa el ADC,y convierte en valor digital de salida del ADC en LUX.
 */


/*
 * LDR.h
 *
 *  Created on: 25 may. 2022
 *      Author: MAGALI KRISTAFOR :)
 */

#ifndef MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_LDR_H_
#define MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_LDR_H_


/*==================[inclusions]=============================================*/
#include <stdint.h>
#include <analog_io.h>

/*==================[external functions declaration]=========================*/


/**
 * @fn LDR_Init(uint8_t Chanel)
 * @brief Inicializa el LDR.
 * @param[in] recibe como parametro el canal para el ADC, el cual es de tipo uint8_t.
 */
void LDR_Init(uint8_t Chanel);



/**
 * @fn LDR_ReadLuxIntensity (uint32_t *ilum_lux)
 * @brief Lee la intensidad de la luz que sensa.
 * @param[in] recibe como parametro un puntero con el valor sensado, el cual es de tipo uint32_t.
 * @param[out] Devuelve verdadero si leyo el dato.
 */
bool LDR_ReadLuxIntensity (uint32_t *ilum_lux);









#endif /* MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_LDR_H_ */
