/* Copyright 2017, Agustin Bassi.
 * All rights reserved.
 *
 * This file is part sAPI library for microcontrollers.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* Date: 2017-30-10 */

#ifndef _SAPI_STDIO_H_
#define _SAPI_STDIO_H_

/*==================[inclusions]=============================================*/

#include "uart.h"

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/** 
 * @fn char* Uart_itoa(uint32_t val, uint8_t base)
 * @brief Conversor de entero a ASCII.
 * @param[in] val Valor entero que se desea convertir.
 * @param[in] base Base sobre la cual se desea realizar la conversion (2: binario, 10: decimal, 16: hexadecimal).
 * @return Puntero al string con el valor convertido
 */
uint8_t* Uart_itoa(uint32_t val, uint8_t base);

/**
 * @fn uint8_t UART_ftoa(uint8_t W, uint8_t D, float data_n)
 * @brief Función para pasar de flotante a ASCII.
 * @param[in] W Cantidad de cifras a la izquierda del punto decimal.
 * @param[in] D Cantidad de cifras a la derecha del punto decimal.
 * @param[in] data_n Valor a convertir.
 * @return Puntero al string con el valor convertido
 */
uint8_t* UART_ftoa(uint8_t W, uint8_t D, float data_n);

/**
 * Configura la UART por la que va a salir el printf.
 * @param uartPrintf UART para printf
 * Opciones: SERIAL_PORT_PC, SERIAL_PORT_P2_CONNECTOR, SERIAL_PORT_RS485
 * @return TRUE
 */
bool stdioConfig(uint8_t port);

/**
 * Realiza un printf sobre la UART seleccionada
 * @param uartPrintf UART para printf
 * Opciones: SERIAL_PORT_PC, SERIAL_PORT_P2_CONNECTOR, SERIAL_PORT_RS485
 * @param format el string formateado con argumentos.
 * @return TRUE si salio bien, FALSE caso contrario.
 */
int stdioPrintf(uint8_t port, const char *format, ...);

/**
 * Realiza un prinft sobre un array.
 * Esta funcion es una adaptacion de la funcion
 * sprintf de la libreria standard de C.
 * @param out el array a guardar los datos.
 * @param format el string formateado con argumentos.
 * @return TRUE si salio bien, FALSE caso contrario.
 */
int stdioSprintf(char *out, const char *format, ...);

/*==================[end of file]============================================*/

#endif /* #ifndef _SAPI_STDIO_H_ */
