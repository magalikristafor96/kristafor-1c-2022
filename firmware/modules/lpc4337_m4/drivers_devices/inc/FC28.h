/* Copyright 2019,
 * Electrónica Programable
 * jmrera@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*Descripcion General.
 * Este dispositivo mide los niveles de humedad en la tierra, de manera proporcional, los valores que arrojan van desde 0 sumergido en agua y a 1024 en aire(seco).
 * De manera que para una mejor visualizacion lo expresado en procentajes; 0% (seco) y el 100% (humedo).
 *
 * El mismo necesita conectarse a otro dispositivo, el cual es un conversor ADC/DAC, que en base al sensado del FC28 nos provee una salida analogica o digital.
 */


/*
 * FC28.h
 *
 *  Created on: 19 may. 2022
 *      Author: MAGALI KRISTAFOR :)
 */


#include <stdint.h>
#include <analog_io.h>



/**
 * @fn FC28_Init(uint8_t Chanel)
 * @brief Inicializa el FC28.
 * @param[in] recibe como parametro el canal para el ADC, el cual es de tipo uint8_t.
 */
void FC28_Init(uint8_t Chanel);


/**
 * @fn bool FC28_ReadHumidity()
 * @brief Lee nivel de humedad de la tierra.
 * @param[in] recibe como parametro un puntero con el valor sensado, el cual es de tipo uint8_t.
 * @param[out] Devuelve verdadero si leyo el dato.
 */
bool FC28_ReadHumidity(uint8_t *humedad);






#ifndef MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_FC28_H_
#define MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_FC28_H_





#endif /* MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_FC28_H_ */
