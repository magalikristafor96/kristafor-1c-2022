/* Copyright 2021,
 * Levrino Micaela
 * micaela_levrino@hotmail.com
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef MMA7260Q_H
#define MMA7260Q_H


/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup MMA7260Q MMA7260Q
 ** @{ */

/** \brief driver que maneja la lectura de datos obtenida a partir de un acelerómetro.
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *  ML	        Micaela Levrino
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20210609 v0.0.1 initials initial version
 */

/*==================[inclusions]=============================================*/

#include "gpio.h"
#include "analog_io.h"
#include "bool.h"

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/
/** @fn bool MMA7260QInit(gpio_t gSelect1, gpio_t gSelect2)
 * @brief Función que inicializa los pines de selección del driver
 * @param[in] gSelect1
 * @param[in] gSelect2
 * @return 1 (true) if no error
 */
bool MMA7260QInit(gpio_t gSelect1, gpio_t gSelect2);
/** @fn float ReadXValue()
 * @brief Función que lee el pin x del driver y devuelve el valor convertido de analógico a digital en unidades de gravedad.
 * @param[in] No hay parámetros
 * @return Aceleración en el eje x
 */
float ReadXValue();
/** @fn float ReadYValue()
 * @brief Función que lee el pin y del driver y devuelve el valor convertido de analógico a digital en unidades de gravedad.
 * @param[in] No hay parámetros
 * @return Aceleración en el eje y
 */
float ReadYValue();
/** @fn float ReadZValue()
 * @brief Función que lee el pin z del driver y devuelve el valor convertido de analógico a digital en unidades de gravedad.
 * @param[in] No hay parámetros
 * @return Aceleración en el eje z
 */
float ReadZValue();
/** @fn bool MMA7260QDeinit(gpio_t gSelect1, gpio_t gSelect2)
 * @brief Función que de-inicializa los pines de selección del driver
 * @param[in] gSelect1
 * @param[in] gSelect2
 * @return 1 (true) if no error
 */
bool MMA7260QDeinit(gpio_t gSelect1, gpio_t gSelect2);

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */

/*==================[end of file]============================================*/
#endif /* #ifndef MMA7260Q_H */

