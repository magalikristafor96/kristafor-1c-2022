/*! @mainpage Proyecto_3
 *
 * General Description
 *
 * Mide la distancia en cm utilizando un ultrasonidos (HC-SR04).
 * Muestra la misma por un display LCD y por un terminal en la PC (Hterm).
 * Indica con los leds los valores dentro de los rangos establecidos correspondientes.
 *
 *
 * This section describes how the program works.
 *
 * La aplicación permite :
 *
 * -Activar y detener la medición por:
 * 									  -TEC1
 * 									  -Puerto serie, en la PC, con el caracter 'O' (On/Off).
 *
 *
 * -Mantener el resultado (“HOLD”) por:
 * 									   -TEC2.
 * 									   -Puerto serie, en la PC, con el caracter 'H' (HOLD).
 *
 * °Se controlan ambas entradas (tecla o PC) por interrupciones.
 *
 *
 * -Medir los siguientes rangos de mediciones y realizar las siguientes acciones:
 *
 * Rangos :                       Acciones:
 * [0-10] cm            		  Muestra en display, en el terminal de la PC enciende LED_RGB_B (Azul).
 * [10-20] cm					  Muestra en display, en el terminal de la PC enciende el LED_RGB_B (Azul) y LED_1.
 * [20-30] cm 					  Muestra en display, en el terminal de la PC enciende el LED_RGB_B (Azul), LED_1 y LED_2.
 * Mayor a 30 cm 				  Muestra en display, en el terminal de la PC enciende el LED_RGB_B (Azul), LED_1, LED_2 y LED_3.
 *
 * En todos los casos se muestra la medicion cada 1 segundo (tiempo de refresco).
 * °Se controlan los timers con interrupciones.
 *
 * Nota: El formato de como se muestra en PC es: "     cm" + " \r\n".
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 *
 *
 * hardConn Hardware Connection
 *
 * |  Ultrasonidos  |				|
 * |	HC-SR04     |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	Echo	 	| 	GPIO_T_FIL0	|
 * | 	Trigger	 	| 	GPIO_T_FIL2	|
 * | 	GND		 	| 	GND			|
 * | 	VCC		 	| 	VCC			|
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 28/04/2022 | Document creation		                         |
 * | 			| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @ Kristafor Magali :)
 *
 */

/*==================[inclusions]=============================================*/
#include "template.h"       /* <= own header */
#include "uart.h"
#include "hc_sr04.h"
#include "switch.h"
#include "gpio.h"
#include "lcditse0803.h"
#include "led.h"
#include "timer.h"
#include "systemclock.h"
#include "uart_stdio.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/
bool on_off_flag = false;
bool hold_flg = false;
bool timer_flg = true;


/*==================[internal functions declaration]=========================*/

void Timer(void) {
	timer_flg = true;
}

void Tecla1(void) {
	on_off_flag = !on_off_flag;
}

void Tecla2(void) {
	hold_flg = !hold_flg;
}

void My_uart(void) {

	uint8_t valor;
	UartReadByte(SERIAL_PORT_PC, &valor);

	switch (valor) {
	case 'o':
		on_off_flag = !on_off_flag;
		break;
	case 'h':
		hold_flg = !hold_flg;
		break;

	}

}

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

void Muestra(uint16_t distancia) {

	LcdItsE0803Write(distancia);

	if ((distancia >= 0) && (distancia <= 10)) {

		LedOn(LED_RGB_B);
		LedOff(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);

	}
	if ((distancia > 10) && (distancia <= 20)) {
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);

	}
	if ((distancia > 20) && (distancia <= 30)) {
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOn(LED_2);
		LedOff(LED_3);

	}
	if (distancia > 30) {
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOn(LED_2);
		LedOn(LED_3);

	}

	UartSendString(SERIAL_PORT_PC, Uart_itoa(distancia, 10));
	UartSendString(SERIAL_PORT_PC, " cm \r\n");

}

int main(void) {
	/* initializations */

	SystemClockInit();
	SwitchesInit();
	LedsInit();
	HcSr04Init(GPIO_T_FIL0, GPIO_T_FIL2);
	LcdItsE0803Init();

	timer_config my_timer_config = {
			TIMER_A,
			1000,
			Timer };

	TimerInit(&my_timer_config);
	TimerStart(my_timer_config.timer);


	serial_config my_uart = {
			SERIAL_PORT_PC,
			115200,
			My_uart };
	UartInit(&my_uart);


	uint16_t distancia;

	SwitchActivInt(SWITCH_1, &Tecla1);

	SwitchActivInt(SWITCH_2, &Tecla2);

	while (1) {
		/* main loop */

		if (timer_flg) {

			if (on_off_flag == true) {

				distancia = HcSr04ReadDistanceInCentimeters();

				if (hold_flg == false) {
					Muestra(distancia);
				}
			} else {

				LedOff(LED_RGB_B);
				LedOff(LED_1);
				LedOff(LED_2);
				LedOff(LED_3);
				distancia = 0;
				Muestra(distancia);
			}

			timer_flg = false;
		}

	}

	/* program should never reach this line */
	return 0;
}

/*==================[end of file]============================================*/

