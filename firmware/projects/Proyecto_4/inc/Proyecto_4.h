/* Copyright 2019,
 * Electrónica Programable
 * jmrera@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*! @mainpage Proyecto_4
 *
 * General Description
 *
 * La aplicacion opera como un osciloscopio.
 *
 *
 * This section describes how the program works.
 *
 * La aplicacion digitaliza una señal analógica y la transmite a un graficador de puerto serie de la PC (Serial-Oscilloscope).
 * Nota: Se muestran los datos convertidos en la PC con el sigueinte formato ("11,22,33\r").
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 *
 * |  Potenciometro |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN 1	 	| 	 DAC		|
 * | 	PIN 2	 	| 	 CH1		|
 * | 	GND		 	| 	 GNDA		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 05/05/2022 | Document creation		                         |
 * | 			| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @ Kristafor Magali :)
 *
 */

#ifndef _PROYECTO_4_H
#define _PROYECTO_4_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/


/**@fn void TimerADC(void)
 * @brief Interrupcion del timer ADC. Empieza la converision analogico-digital con una frecuencia de muestreo de 500Hz (T=2ms).
*/
void TimerADC(void);

/**@fn void TimerECG(void)
 * @brief Interrupcion del timer ECG. Realiza la converison digital-analogico. Construye la señal de ECG a partir de los valores del vector,ecg[], a una frecuencia de 250Hz(T=4ms).
*/
void TimerECG(void);

/**@fn void LeoMuestra(void)
 * @brief Interrupcion del ADC. Realiza la lectura de los datos previamente convertidos de analogio-digital y los muestra por un terminal de la PC.
*/
void TimerECG(void);


/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

