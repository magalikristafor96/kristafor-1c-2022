/*! @mainpage Puerto
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 * La aplicacion debe encender o apagar el LED 2 cuando detecta el flanco ascendente de una señal de reloj de 0.5 Hz.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Arduino		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	D4		 	| 	TCOL_1 		|
 * | 		 		| 				|
 * | 			 	| 				|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 27/04/2022 | Document creation		                         |
 * | 			| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @ Kristafor Magali :)
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/puerto.h"       /* <= own header */

#include "led.h"
#include "gpio.h"
#include "systemclock.h"
#include "stdio.h"
#include "stdint.h"


/*==================[macros and definitions]=================================*/

bool EstadoActual_flg=false; 	//Estado Actual.
bool EstadoAnterior_flg=false; 	//Estado Anterior.

/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){
	/* initializations */
	SystemClockInit();
	LedsInit();
	GPIOInit(GPIO_T_COL1, GPIO_INPUT);


	LedOff(LED_RGB_R);
	LedOff(LED_RGB_B);
	LedOff(LED_RGB_G);
	LedOff(LED_1);
	LedOff(LED_3);

	
    while(1){
		/* main loop */


    	EstadoActual_flg=GPIORead(GPIO_T_COL1);

    	if(EstadoActual_flg==true && EstadoAnterior_flg==false){
    		LedToggle(LED_2);
    		}
    	EstadoAnterior_flg=EstadoActual_flg;
	}
    
	/* program should never reach this line */
	return 0;
}

/*==================[end of file]============================================*/

