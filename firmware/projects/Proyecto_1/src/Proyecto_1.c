/*! @mainpage Proyecto_1
 *
 * Descripcion general
 *
 * Mide la distancia en cm utilizando un ultrasonidos HC-SR04, muestra la misma por un display LCD e indica rangos medidos con los leds.
 *
 *
 * This section describes how the program works.
 *
 * La aplicación permite :
 *
 * -Activar y detener la medición con TEC1.
 * -Mantener el resultado (“HOLD”) con TEC2.
 *
 * -Medir los siguientes rangos y realizar las siguientes acciones:
 *
 * Rangos :                       Acciones:
 * [0-10] cm            		  Muestra en display y enciende LED_RGB_B (Azul).
 * [10-20] cm					  Muestra en display y enciende el LED_RGB_B (Azul) y LED_1.
 * [20-30] cm 					  Muestra en display y enciende el LED_RGB_B (Azul), LED_1 y LED_2.
 * Mayor a 30 cm 				  Muestra en display y enciende el LED_RGB_B (Azul), LED_1, LED_2 y LED_3.
 *
 * En todos los casos se muestra la medicion cada 1 segundo (tiempo de refresco).
 *
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |  Ultrasonidos  |				|
 * |	HC-SR04     |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	Echo	 	| 	GPIO_T_FIL0	|
 * | 	Trigger	 	| 	GPIO_T_FIL2	|
 * | 	GND		 	| 	GND			|
 * | 	VCC		 	| 	VCC			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 07/04/2022 | Document creation		                         |
 * |			| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @ Kristafor Magali
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto_1.h"       /* <= own header */
#include "hc_sr04.h"
#include "switch.h"
#include "gpio.h"
#include "lcditse0803.h"
#include "led.h"
#include "delay.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/




void Muestra(uint16_t distancia){

	LcdItsE0803Write(distancia);

	if( (distancia>=0)&&(distancia<=10) ){

	    		LedOn(LED_RGB_B);
	    		LedOff(LED_1);
	    	    LedOff(LED_2);
	    		LedOff(LED_3);

	    		}
	    	if( (distancia>10) && (distancia<=20) ){
	    		LedOn(LED_RGB_B);
	    		LedOn(LED_1);
	    		LedOff(LED_2);
	    		LedOff(LED_3);

	    	}
	    	if( (distancia>20) && (distancia<=30) ){
	    		LedOn(LED_RGB_B);
	    		LedOn(LED_1);
	    		LedOn(LED_2);
	    		LedOff(LED_3);

	    	}
	    	if( distancia>30 ){
	    		LedOn(LED_RGB_B);
	    		LedOn(LED_1);
	    		LedOn(LED_2);
	    		LedOn(LED_3);

	    	}
}




int main(void){
	/* initializations */

	bool on_off_flag=false;
	bool hold_flg=false;
	
	SystemClockInit();
	
	SwitchesInit();
	LedsInit();
	HcSr04Init (GPIO_T_FIL0, GPIO_T_FIL2);
	LcdItsE0803Init();

	uint16_t distancia;



    while(1){
		/* main loop */

    	uint8_t teclas;
    	uint16_t valor;

    	teclas = SwitchesRead();
    	DelayMs(200);
    	switch(teclas){

    	case SWITCH_1:

    		on_off_flag=!on_off_flag;
    		break;

    	case SWITCH_2:

    		hold_flg=!hold_flg;
    		break;
    	}

    	if(on_off_flag==true){

    		distancia=HcSr04ReadDistanceInCentimeters();

    		if(hold_flg==false){
    			Muestra(distancia);
    		}
    	}
		else{
			distancia=0;
			Muestra (distancia);
		}

    	DelayMs(1);

	}
    


	/* program should never reach this line */
	return 0;
}

/*==================[end of file]============================================*/

