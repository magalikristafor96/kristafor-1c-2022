/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Name
 *
 */

/*==================[inclusions]=============================================*/
#include "template.h" /* <= own header */
#include "stdint.h"
#include "gpio.h"
/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/

typedef struct
{
	gpio_t pin;
	io_t dir;
} gpioConf_t;

/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/



void BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number ){

	uint32_t i=0;

	while(i<=digits){
		bcd_number[i]= data%10;
		data= data/10;
		i++;
	}


}


void BCDtoGpio (gpioConf_t * conf , uint8_t bcd){



	for(int i=0; i<4; i++){

		GPIOInit(conf[i].pin, conf[i].dir);
	}

	for( int i=0; i<4; i++){

		GPIOState(conf[i].pin, (bcd&(1<<i))>>i);
	}

}


void MostrarDisplay (uint32_t dato, uint8_t cdig, gpioConf_t *conf1 , gpioConf_t *conf2 ){


	uint8_t vectbcd [cdig];

    BinaryToBcd(dato,cdig,vectbcd);

	for(int j=0; j<cdig; j++){

		BCDtoGpio (conf1, vectbcd[j]);
		GPIOInit(conf2[j].pin, conf2[j].dir);
		GPIOOn(conf2[j].pin);
		GPIOOff(conf2[j].pin);
	}


}





int main(void){
	/* initializations */
	
	uint8_t cant=3;

	uint32_t dato= 123;



	gpioConf_t lcd[4]={{GPIO_LCD_1, GPIO_OUTPUT},
				{GPIO_LCD_2, GPIO_OUTPUT},
				{GPIO_LCD_3, GPIO_OUTPUT},
				{GPIO_LCD_4, GPIO_OUTPUT}};

	gpioConf_t lcdOut [3]={{GPIO_5, GPIO_OUTPUT},
	{GPIO_3, GPIO_OUTPUT},
	{GPIO_1, GPIO_OUTPUT}};

	MostrarDisplay (dato, cant, lcd, lcdOut );


    while(1){
		/* main loop */

	}
    
	/* program should never reach this line */
	return 0;
}

/*==================[end of file]============================================*/

