/*! @mainpage Template
 *
 * \section genDesc General Description
 * La aplicacion permite medir el nivel de agua en dos tanques, destinados para el riego de un cultivo.
 * This section describes how the program works.
 *
 * Segun el nivel de agua en los tanques, acciona una bomba para el llenado del tanque 2
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * hardConn Hardware Connection
 * |  Bomba		    |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN 1	 	| 	 GPIO_0		|
 * | 	VCC		 	| 	 VCC		|
 * | 	GND		 	| 	 GNDA		|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 16/06/2022 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Magali Kristafor :)
 *
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

