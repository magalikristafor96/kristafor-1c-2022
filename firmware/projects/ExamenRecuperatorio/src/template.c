/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Name
 *
 */

/*==================[inclusions]=============================================*/
#include "template.h"       /* <= own header */
#include "uart.h"
#include "uart_stdio.h"
#include "analog_io.h"
#include "timer.h"
#include "systemclock.h"
#include "bool.h"

#include "gpio.h"
#include "nivel_tanque.h"

#include "FC28.h"

/*==================[macros and definitions]=================================*/


bool flg_agua1=false;
bool flg_agua2=false;
bool flg_humedad=false;


bool on_off_flag=false;


uint16_t tanque1;
uint16_t tanque2;

uint8_t valor_humedad;


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/



void MedirNivelAgua(){

	flg_agua1=ReadNivelAgua(&tanque1);
	flg_agua2=ReadNivelAgua(&tanque2);

}


void MedirHumedad(){

	flg_humedad=TRUE;
	valor_humedad=(valor_humedad*100)/1024;
	valor_humedad= (float)(valor_humedad*85/3.3);
}

void My_uart(){

	uint8_t valor;
	UartReadByte(SERIAL_PORT_PC, &valor);

		switch (valor) {

		case 'I':
			on_off_flag = true;
			break;

		case 'O':
			on_off_flag= false;
			break;

}



int main(void){
	/* initializations */
	SystemClockInit();
	GPIOInit(GPIO_0, GPIO_OUTPUT);
	FC28_Init(CH2);


	//Inicializo el timer de la humedad
	timer_config Timer_Humedad={
				TIMER_A,
				1000, // 1s
				MedirHumedad
		};
		TimerInit(&Timer_Humedad);
		TimerStart(Timer_Humedad.timer);



	//Inicializo el timer del Agua
	timer_config Timer_Agua={
					TIMER_B,
					1000, // 1s
					MedirNivelAgua
			};
			TimerInit(&Timer_Agua);
			TimerStart(Timer_Agua.timer);



	analog_input_config	FC28={
			CH2,
			AINPUTS_SINGLE_READ,
			NULL
	};
	AnalogInputInit(&FC28);
	AnalogInputReadPolling(CH2,&valor_humedad);


	//Inicializo la Uart

	serial_config my_uart={
			SERIAL_PORT_PC,
			115200,
			My_uart
			};
	UartInit(&my_uart);

	
    while(1){
		/* main loop */

    	if(on_off_flag==TRUE){

    		//Agua

    		if(flg_agua1==TRUE && flg_agua2==TRUE){
    			if(  (tanque2<600) && (tanque1>=200) ){

    				GPIOOn(GPIO_0);

    				//Activo bomba
    			}else{
    				if(  (tanque2==900) && (tanque1<200)){
    					//desactivo bomba
    					GPIOOff(GPIO_0);

    				}
    			}
    		}


    		//humedad

    		if(flg_humedad==TRUE ){

    		//Muestro

    		UartSendString(SERIAL_PORT_PC, "Tanque 1: ");
    		UartSendString(SERIAL_PORT_PC, "[");
    		UartSendString(SERIAL_PORT_PC, Uart_itoa(tanque1, 10) );
    		UartSendString(SERIAL_PORT_PC, "]");
    		UartSendString(SERIAL_PORT_PC, "litros");


    		UartSendString(SERIAL_PORT_PC, "Tanque 2: ");
    		UartSendString(SERIAL_PORT_PC, "[");
    		UartSendString(SERIAL_PORT_PC, Uart_itoa(tanque2, 10) );
    		UartSendString(SERIAL_PORT_PC, "]");
    		UartSendString(SERIAL_PORT_PC, "litros");

    		UartSendString(SERIAL_PORT_PC, "Humedad del suelo:");
    		UartSendString(SERIAL_PORT_PC, "[");
    		UartSendString(SERIAL_PORT_PC, Uart_itoa(valor_humedad, 10) );
    		UartSendString(SERIAL_PORT_PC, "]");
    		UartSendString(SERIAL_PORT_PC, "%");

    		}

    	}

    	else{
    		//Apagar todo


    	}

	}
    
	/* program should never reach this line */
	return 0;
}

/*==================[end of file]============================================*/

