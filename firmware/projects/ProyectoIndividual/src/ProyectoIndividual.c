/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Name
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/ProyectoIndividual.h"       /* <= own header */
#include "uart.h"
#include "uart_stdio.h"
#include "analog_io.h"
#include "timer.h"
#include "systemclock.h"

#include "gpio.h"
#include "uart.h"
#include "LDR.h"
#include "NeoPixel.h"
#include "led.h"
/*==================[macros and definitions]=================================*/


//banderas

bool flg_ldr=false;
bool flg_fc28=false;

bool on_off_flag = false;
bool manual_auto_flg = false;

// variables de luz
uint32_t valor_lux;
uint32_t error;
float k=0.5;  // experimental
uint8_t outControlador;

uint8_t brillo=30;


//variables de humedad

uint8_t valor_humedad;


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/


// interrupciones //

// interrupcion del timer LDR
void MedirLuz(){
	flg_ldr=LDR_ReadLuxIntensity(&valor_lux);
}


// interrupcion timer Humedad
void SensarHumedad(){

	flg_fc28=FC28_ReadHumidity(&valor_humedad);
	UartSendString(SERIAL_PORT_P2_CONNECTOR, "*B");
	if(valor_humedad == 0){
		UartSendString(SERIAL_PORT_P2_CONNECTOR, "0");
	} else{
		UartSendString(SERIAL_PORT_P2_CONNECTOR, Uart_itoa(valor_humedad, 10));
	}


}

void My_uart(void) {

	uint8_t valor;
	UartReadByte(SERIAL_PORT_P2_CONNECTOR, &valor);

	switch (valor) {

	case 'C':
		on_off_flag = true;
		break;

	case 'c':
		on_off_flag= false;
		break;

	case 'A':
		manual_auto_flg =true;
		break;

	case 'a':
		manual_auto_flg =false;
		break;

	case 'U':
		if(manual_auto_flg==FALSE){
			brillo+=5;
			if(brillo>255){
				brillo=255;
			}
		}
		break;

	case 'D':

		if(manual_auto_flg==FALSE){
			brillo-=5;

			if(brillo<5){
				brillo=5;
			}
		}
		break;

	}

}






int main(void){
	/* initializations */
	
	SystemClockInit();
	LDR_Init(CH1);
	NeoPixelInit(8, Anillo_Max2);
	FC28_Init(CH2);
	LedsInit();


	// inicializo timer de LDR
	timer_config Timer_LDR={
			TIMER_B,
			1000, // 1s
			MedirLuz
	};
	TimerInit(&Timer_LDR);
	TimerStart(Timer_LDR.timer);


	// inicializo un timer para el sensor de humedad
	timer_config Timer_FC28={
				TIMER_A,
				1000,//
				SensarHumedad
		};
	TimerInit(&Timer_FC28);
	TimerStart(Timer_FC28.timer);


	//Inicializo la Uart
	serial_config my_uart={
			SERIAL_PORT_P2_CONNECTOR,
			9600,
			My_uart
	};
	UartInit(&my_uart);


	// Empieza el diagrama de flujo

    while(1){
		/* main loop */

    	if(on_off_flag==TRUE){

    		if(manual_auto_flg==TRUE){

    			//control Luz
    			    		if(flg_ldr==TRUE){

    			    			if(valor_lux < LUX_NORMAL){


    			    			error=LUX_NORMAL-valor_lux;
    			    			outControlador= (uint8_t)(k*(float)error);


    			    			NeoPixelAll(outControlador, outControlador, outControlador);
    			    		}else {
    			    			NeoPixelAllOFF();
    			    		}
    			    		}
    		}
    		else{
    			NeoPixelAll(brillo, brillo, brillo);
    		}



    		//Muestra Humedad/ alerta

    		    	if (flg_fc28==TRUE){


    		    		//Seco
    		    		if( (valor_humedad > 0) && (valor_humedad <=20) ){
    		    			LedOff(LED_RGB_B);
    		    			LedOn(LED_1);
    		    			LedOff(LED_2);
    		    			LedOff(LED_3);
    		    		}

    		    		//Humedo
    		    		if( (valor_humedad>50) && (valor_humedad<100) ){
    		    			LedOff(LED_RGB_B);
    		    			LedOff(LED_1);
    		    			LedOn(LED_2);
    		    			LedOff(LED_3);
    		    		}

    		    		//Normal
    		    		if( (valor_humedad>20) && (valor_humedad<50) ){
    		    			LedOff(LED_RGB_B);
    		    			LedOff(LED_1);
    		    			LedOff(LED_2);
    		    			LedOff(LED_3);
    		    		}
    		    	}
    	}

    	else{
    		NeoPixelAllOFF();
    		valor_humedad=0;
    		LedOff(LED_RGB_B);
    		LedOff(LED_1);
    		LedOff(LED_2);
    		LedOff(LED_3);
    	}


    }
	/* program should never reach this line */
	return 0;
}

/*==================[end of file]============================================*/

