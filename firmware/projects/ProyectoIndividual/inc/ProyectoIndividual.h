/* Copyright 2019,
 * Electrónica Programable
 * jmrera@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */


/*! @mainpage ProyectoIndividual
 *
 * \section genDesc General Description
 *
 *
 * This section describes how the program works.
 * La aplicaccion sensa los diferentes parametros necesarios para la supervivencia de una planta. En base a ellos brinda una determinada respuesta.
 * La misma se conecta por medio de bluetooth a una app en el celular con la cual podemos comandar el encendido y apagado de nuestra aplicacion y mediante botones regular la intensidad de la luz.
 *
 * Entre los sensores con los que cuenta nuestra app tenemos:
 * Sensores:
 * 			LDR --------> Sensa Intensidad proporcional de luz
 * 			FC28--------> Sensa Humedad de la tierra
 *
 *
 * Entre los actuadores con los que cuenta nuestra app tenemos:
 * Actuadores:
 * 			NeoPixel------>Nos provee las diferente intensidades de Luz
 * 			Leds----------> Indica si la tierra es seca (Led1/rojo) y si la tierra esta humedad (led2/azul).
 *
 *
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 \section hardConn Hardware Connection
 *
 *
 * |    NeoPixel 	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |    PIN         |   GPIO_08		|
 * | 	GND		 	| 	GND			|
 * | 	VCC		 	| 	VCC			|
 *
 *
 *|    LDR		 	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |    PIN         |   CH1			|
 * | 	GND		 	| 	GND			|
 * | 	VCC		 	| 	VCC			|
 *
 *
 *
 * |   FC28		 	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |    PIN         |   CH2			|
 * | 	GND		 	| 	GND			|
 * | 	VCC		 	| 	VCC			|
 *
 *
 * |    Bluetooth 	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |	TX			|   RX   		|
 * |    RX          |   TX          |
 * | 	GND		 	| 	GND			|
 * | 	VCC		 	| 	VCC			|
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 19/05/2022 | Document creation		                         |
 * | 			| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @ Magali Kristafor :)
 *
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

#include "uart.h"
#include "uart_stdio.h"
#include "analog_io.h"
#include "timer.h"
#include "systemclock.h"
#include "FC28.h"
#include "LDR.h"


int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

