/* Copyright 2019,
 * Electrónica Programable
 * jmrera@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*! @mainpage Proyecto_2
 *
 * General Description
 *
 * Mide la distancia en cm utilizando un ultrasonidos HC-SR04.
 * Muestra la misma por un display LCD.
 * Indica con los leds los valores dentro de los rangos correspondientes.
 *
 *
 * This section describes how the program works.
 *
 * La aplicación permite :
 *
 * -Activar y detener la medición con TEC1.
 * -Mantener el resultado (“HOLD”) con TEC2.
 *  °Se controlan las teclas por interrupciones.
 *
 * -Medir los siguientes rangos de mediciones y realiza las siguientes acciones:
 *
 * Rangos :                       Acciones:
 * [0-10] cm            		  Muestra en display y enciende LED_RGB_B (Azul).
 * [10-20] cm					  Muestra en display y enciende el LED_RGB_B (Azul) y LED_1.
 * [20-30] cm 					  Muestra en display y enciende el LED_RGB_B (Azul), LED_1 y LED_2.
 * Mayor a 30 cm 				  Muestra en display y enciende el LED_RGB_B (Azul), LED_1, LED_2 y LED_3.
 *
 * En todos los casos se muestra la medicion cada 1 segundo (tiempo de refresco).
 * °Se controlan los timers con interrupciones.
 *
 *
 *
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |  Ultrasonidos  |				|
 * |	HC-SR04     |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	Echo	 	| 	GPIO_T_FIL0	|
 * | 	Trigger	 	| 	GPIO_T_FIL2	|
 * | 	GND		 	| 	GND			|
 * | 	VCC		 	| 	VCC			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 28/04/2022 | Document creation		                         |
 * | 			| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @ Kristafor Magali :)
 *
 */

#ifndef _PROYECTO_2_H
#define _PROYECTO_2_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/**
 * @fn void Tecla2(void)
 * @brief Interrupcion de la Tecla 2. Cambia el estado de la bandera
 *
 */
void Tecla2(void);

/**
 * @fn void Tecla1(void)
 * @brief Interrupcion de la Tecla 1.
 */
void Tecla1(void);

/**
 * @fn void Timer(void)
 * @brief Interrupcion del timer.
 */
void Timer(void);

/**
 * @fn void Muestra(uint16_t distancia)
 * @brief Muestra por display LCD e indica con los Leds, prendiendo o apagando, los rangos de distancias correspondientes.
 * @param[in] recibe como parametro el valor de la distancia medida, la cual es de tipo uint16_t.
 */
void Muestra(uint16_t distancia);


/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

