/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Name
 *
 */

/*==================[inclusions]=============================================*/
#include "template.h"       /* <= own header */
#include "stdint.h"
#include "led.h"

/*==================[macros and definitions]=================================*/

# define ON 1
# define OFF 2
# define TOGGLE 3
/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/


struct leds
{
	uint8_t n_led;
	uint8_t n_ciclos;
	uint32_t periodo;
	uint8_t mode;
} my_leds;


void ManejoLed(struct leds *my_leds) {

	switch (my_leds->mode) {

	case ON:
		switch (my_leds->n_led) {
		case LED_1:
			LedOn(my_leds->n_led);
			break;

		case LED_2:
			LedOn(my_leds->n_led);
			break;

		case LED_3:
			LedOn(my_leds->n_led);
			break;
		}
		break;

	case OFF:
		switch (my_leds->n_led) {
		case LED_1:
			LedOff(LED_1);
			break;

		case LED_2:
			LedOff(LED_2);
			break;

		case LED_3:
			LedOff(LED_3);
			break;
		}
		break;

	case TOGGLE:
		for (int i = 0; i < my_leds->n_ciclos; i++) {
			switch (my_leds->n_led) {
			case LED_1:
				LedToggle(LED_1);
				break;
			case LED_2:
				LedToggle(LED_2);
				break;
			case LED_3:
				LedToggle(LED_3);
				break;
			}
			for (uint32_t j = 0; j < my_leds->periodo; j++) {
						asm ("nop");

					}
		}
		break;


	}

}



int main(void){
	/* initializations */
	
	struct leds my_leds;
	my_leds.mode=TOGGLE;
	my_leds.n_ciclos=20;
	my_leds.periodo=5000000;
	my_leds.n_led=LED_1;
	LedsInit();
	ManejoLed(&my_leds);





	
    while(1){
		/* main loop */

	}
    
	/* program should never reach this line */
	return 0;
}

/*==================[end of file]============================================*/

