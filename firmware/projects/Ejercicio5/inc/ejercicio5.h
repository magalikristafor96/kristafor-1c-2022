/*! @mainpage Template
 * Ejercicio 5 Guia 1
 *
 * \section genDesc General Description
 *
 * Carga un dato BCD en los pines para eso chequea los estados de los pines.
 *
 *
 * This section describes how the program works.
 *
 * El programa contiene una función la cual recibe como parametros, el dato BCD y
 * un vector de estructuras del tipo gpioConf_t.
 *
 * La funcion establece en qué valor colocar cada bit del dígito BCD e
 * indexa el vector anterior, operando sobre el puerto y el pin que corresponde.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Name
 *
 *
 *
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif
#include "gpio.h"




int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

