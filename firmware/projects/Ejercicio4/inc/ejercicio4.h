/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 *Conversion de un dato BCD a binario.
 *

 *This section describes how the program works.
 *
 *El programa contiene la funcion, BinaryToBcd, la cual recibe como parametros
 *  - un dato de 32 bits,
 *  -cantidad de dígitos de salida
 *  -un puntero a un arreglo donde se almacene los n dígitos.
 *
 *  La función convierte el dato recibido a BCD, guardando cada uno de los
 *  dígitos de salida en el arreglo pasado como puntero.
 *
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Name
 *
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif
#include "stdint.h"


int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

